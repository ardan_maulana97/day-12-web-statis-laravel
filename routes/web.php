<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/home', 'HomeController@home')->name('home');
Route::get('/register', 'AuthController@form')->name('register');
Route::post('/register', 'AuthController@register')->name('daftar');
Route::post('/welcome', 'AuthController@welcome')->name('welcome');
Route::get('user/{id}', function($id) {
    return view('users.profile', User::find($id));
});
