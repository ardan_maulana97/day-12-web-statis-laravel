<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1><b>Buat Account Baru!</b></h1>
    <h2><b>Sign Up Form</b></h2>
    
    <form action="/register" method="POST">
    @csrf 
        <label for=""> First Name : </label><br><br>
            <input type="text" name="first_name"><br><br>
        <label for=""> Last Name : </label><br><br>
            <input type="text"name="last_name"><br><br>
        <label for="">Gender :</label><br><br>
        <input type="radio" name="Gender">Male
        <label for=""></label><br>
        <input type="radio" name="Gender">Female
        <label for=""></label><br>
        <input type="radio" name="Gender">Other <br><br>
        <label for="">Nasionality :</label><br><br>
        <select name="Nasionality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label for="">Language Spoken :</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Arabic <br>
        <input type="checkbox">Japanese <br>
        <input type="checkbox">Other <br><br>
        <label>Bio :</label><br><br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>